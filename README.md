
*Do it hyperscale your digital room and save nature - #DIToHyro*

# Motivation
In [1] bis [3] ist der Ist-Zustand im digitalen Raum dokumentiert, der die Motivation erklärt und die Dringlichkeit des DIToHyro-Vorhabens begründet.

# Alleinstellungsmerkmale der DIToHyro-Infrastruktur
*Green Paper*

1. Die DIToHyro ist ein Konzept und eine Infrastruktur, die von der Solidargemeinschaft von Menschen, die überzeugt sind, dass **ein selbstbestimmtes Leben im digitalen Raum zu führen *ein Menschenrecht* ist**, getragen wird. Ein Recht, dass keinesfalls vom Profitstreben der Wirtschaftsakteure und von der Neugierde des Staates abhängig sein darf.
2. die **Hochverfügbarkeit, Ausfallsicherheit** der DIToHyro-Infrastruktur wird nicht nur durch verwendete technische Lösungen, sondern im Wesentlichen durch die Zuverlässigkeit und Vertrauenswürdigkeit der Kommunikationspartner, der Teilnehmer der Solidargemeinschaft, die sich dem *Verhaltenskodex der DIToHyro-Teilnehmer* verpflichtet haben, bestimmt
3. DIToHyro-Infrastruktur setzt auf robuste, jahrzehntelang erprobte und bewährte Protokolle wie https://, Access Control List (ACL) und das Representational State Transfer (REST) Paradigma.

4. Benutzefreundlichkeit, Barrierefreiheit, Zugänglichkeit der DIToHyro-Dienste selbst und Informationen über diese Dienste:

* mittel- und längefristig gehören dazu telefonischer Support für die DITOHyro-Nutzer:innen, gute Anleitungen und benutzerfreundliche leistungsfähige Chatbots, die die Nutzer:innen bei der Erledigung von Routineaufgaben unterstützen

* Professionell konzipierte Lernmodule in Form von Handbüchern, Trainings

* Online- und Präsenzveranstaltungen für Anfänger und Fortgeschrittene

# DIToHyro-Verhaltenskodex
*DIToHyro-Verhaltenskodex muss noch erarbeitet werden*
# Quellenverzeichnis
[1] DIE SOZIALE NETZWERKE vor der Haustür lebenswert gestalten. *Ist-Zustand*, 27.01.2020 - https://realtime.fyi/articles/nmoplus/die-soziale-netzwerke-vor-der-haustuer-lebenswert-gestalten1

[2] DIE SOZIALE NETZWERKE vor der Haustür lebenswert gestalten. *Aktionsplan*, 27.01.2020 - https://realtime.fyi/articles/nmoplus/die-soziale-netzwerke-vor-der-haustuer-lebenswert-gestalten-aktionsplan1

[3] Nach Datenleck: Hausdurchsuchung statt Dankeschön - eine indirekte Werbeaktion für hochverfügbare Fediverse-Infrastruktur, 18.10.2021 - https://hub.libranet.de/articles/nmoplus/nach-datenleck-hausdurchsuchung-statt-dankeschoen-eine-indirekte-werbeaktion-fuer-hochverfuegbare-fediverse-infrastruktur

[4] Dito - *das Gleiche (wie zuvor)* -  https://de.wikipedia.org/wiki/Dito

[5] UserLAnd: Use Linux Anywhere - https://userland.tech/

[6] Do it hyperscale your digital room and save nature - https://codeberg.org/HappyPony/DIToHyro/ 

# Interessante technische Lösungen
Mit UserLAnd [5] bietet sich die Möglichkeit eine leistungsfähige auf die Bedürfnisse der jeweligen Zielgruppe angepasste Arbeitsumgebung in Gestalt einer UserLAnd-App vorzubereiten, die dann auch von Linux-fremden Nutzer:innen installiert werden kann. 





